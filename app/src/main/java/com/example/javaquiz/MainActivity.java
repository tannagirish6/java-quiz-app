package com.example.javaquiz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private String questions[] = {"Java is a person?", "Java is OOP language?", "Java is only compiled language?", "Java Supports inheritance?", "Java can be used to develop desktop applications?"};
    private boolean answers[] = {false, true, false, true, true};
    private int score = 0;
    private int index = 0;
    Button yes;
    Button no,restart;
    TextView question;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        yes = findViewById(R.id.yes);
        no = findViewById(R.id.no);
        question = findViewById(R.id.textView);
        restart = findViewById(R.id.restart);
        question.setText(questions[index]);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(index <= questions.length-1){
                    if(answers[index]){
                        score++;
                    }
                    index++;
                    if(index <= questions.length-1){
                        question.setText(questions[index]);
                    }else{
                        Toast.makeText(MainActivity.this, "Your score is "+score+"/5", Toast.LENGTH_SHORT).show();
                        restart.setVisibility(View.VISIBLE);

                    }
                }
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(index <= questions.length-1){
                    if(!answers[index]){
                        score++;
                    }
                    index++;
                    if(index <= questions.length-1){
                        question.setText(questions[index]);
                    }else{
                        Toast.makeText(MainActivity.this, "Your score is "+score+"/5", Toast.LENGTH_SHORT).show();
                        restart.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
        restart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                restart.setVisibility(View.INVISIBLE);
                navigateUpTo(new Intent(MainActivity.this, MainActivity.class));
                startActivity(getIntent());
            }

        });
    }
}